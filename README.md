# Spring Boot Docker demo

> Ce projet accompagne [cet article](https://blog.maxds.fr/posts/docker-and-spring-boot-2-3/) paru sur notre blog.

Pour builder une image docker, faites :

```bash
mvnw spring-boot:build-image
```

Pour la lancer :
```bash
docker run --name=docker-spring -p8080:8080 spring-docker:1.0
```

Il est possible de passer une variable d'environnement au conteneur avec :
```bash
docker run --name=docker-spring --tty  -e "MAXDS_NAME=Sarah Connor" -p8080:8080 spring-docker:1.0
```
