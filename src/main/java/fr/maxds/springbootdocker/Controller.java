package fr.maxds.springbootdocker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Controller {

    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    @Value("${maxds.name}")
    private String name;

    @GetMapping("/who-are-you")
    public String whoAreYou() {
        log.info("Request to know who I am...");
        return "My name is " + name + " !";
    }
}
